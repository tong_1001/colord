Name:           colord
Version:        1.4.3
Release:        6
Summary:        A system activated daemon
License:        GPLv2+ and LGPLv2+
URL:            https://www.freedesktop.org/software/colord/
Source0:        https://www.freedesktop.org/software/colord/releases/%{name}-%{version}.tar.xz

BuildRequires:  color-filesystem dbus-devel docbook-utils gettext glib2-devel
BuildRequires:  gobject-introspection-devel gtk-doc libgudev1-devel
BuildRequires:  libxslt meson sqlite-devel systemd systemd-devel vala-tools
BuildRequires:  lcms2-devel >= 2.6 libgusb-devel >= 0.2.2 polkit-devel >= 0.103

Requires:       color-filesystem %{name}-libs = %{version}-%{release}
%{?systemd_requires}
Requires(pre):  shadow-utils
Obsoletes:      colord < 0.1.27-3
Obsoletes:      shared-color-profiles <= 0.1.6-2
Provides:       shared-color-profiles

%description
colord is a system service that makes it easy to manage, install and
generate color profiles to accurately color manage input and output devices.

%package     libs
Summary:     Libraries for %{name}

%description libs
Libraries for %{name}.

%package        devel
Summary:        This devel package contains development files
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-libs = %{version}-%{release}
Obsoletes:      colorhug-client-devel <= 0.1.13 shared-color-profiles-extra <= 0.1.6-2
Obsoletes:      colord-extra-profiles
Provides:       colord-extra-profiles shared-color-profiles-extra

%description    devel
Development package for colord.

%package        help
Summary:        Help documentation package for color
Requires:       %{name} = %{version}-%{release}
Provides:       colord-devel-docs
Obsoletes:      colord-devel-docs

%description    help
This help package contains help documents for color.

%prep
%autosetup -p1

%build
ulimit -Sv 2000000

%meson -Dargyllcms_sensor=false -Dbash_completion=false -Ddaemon_user=colord \
       -Dprint_profiles=false -Dvapi=true
%meson_build

%install
%meson_install
cat >> $RPM_BUILD_ROOT/var/lib/colord/storage.db << EXIT
EXIT
cat >> $RPM_BUILD_ROOT/var/lib/colord/storage.db << EXIT
EXIT
%{_rpmconfigdir}/find-lang.sh %{buildroot} %{name}

%pre
cat /etc/group | grep colord > /dev/null || groupadd -r colord
cat /etc/passwd | grep colord >/dev/null || useradd -r -g colord \
    -s /sbin/nologin -d /var/lib/colord -c "User for colord" colord
exit 0

%preun
%systemd_preun colord.service

%post
%systemd_post colord.service

%postun
%systemd_postun colord.service

%ldconfig_scriptlets libs

%files -f %{name}.lang
%doc AUTHORS
%license COPYING
%{_bindir}/*
%attr(755,colord,colord) %dir %{_localstatedir}/lib/colord/icc
%attr(755,colord,colord) %dir %{_localstatedir}/lib/colord
%{_datadir}/dbus-1/system.d/org.freedesktop.ColorManager.conf
%{_datadir}/dbus-1/system-services/org.freedesktop.ColorManager.service
%{_datadir}/dbus-1/interfaces/org.freedesktop.ColorManager*.xml
%{_datadir}/polkit-1/actions/org.freedesktop.color.policy
%{_datadir}/colord
%{_libdir}/colord-plugins
%{_libdir}/colord-sensors
%{_libexecdir}/colord-session
%{_libexecdir}/colord
%{_unitdir}/colord.service
%{_userunitdir}/colord-session.service
%dir %{_icccolordir}/colord
%{_icccolordir}/colord/AdobeRGB1998.icc
%{_icccolordir}/colord/Bluish.icc
%{_icccolordir}/colord/Rec709.icc
%{_icccolordir}/colord/sRGB.icc
%{_icccolordir}/colord/SMPTE-C-RGB.icc
%{_icccolordir}/colord/ProPhotoRGB.icc
%{_icccolordir}/colord/x11-colors.icc
%ghost %attr(-,colord,colord) %{_localstatedir}/lib/colord/*.db
/usr/lib/udev/rules.d/*.rules
/usr/lib/tmpfiles.d/colord.conf
%{_datadir}/dbus-1/interfaces/org.freedesktop.ColorHelper.xml
%{_datadir}/dbus-1/services/org.freedesktop.ColorHelper.service
%{_datadir}/glib-2.0/schemas/org.freedesktop.ColorHelper.gschema.xml

%files libs
%{_libdir}/libcolord.so.2*
%{_libdir}/libcolordprivate.so.2*
%{_libdir}/libcolorhug.so.2*
%{_libdir}/girepository-1.0/*.typelib

%files devel
%{_includedir}/colord-1
%{_libdir}/libcolord.so
%{_libdir}/libcolorhug.so
%{_libdir}/libcolordprivate.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/vala/vapi/colord.vapi
%{_datadir}/gir-1.0/*.gir
%{_datadir}/vala/vapi/colord.deps
%{_icccolordir}/colord/AppleRGB.icc
%{_icccolordir}/colord/BestRGB.icc
%{_icccolordir}/colord/BetaRGB.icc
%{_icccolordir}/colord/BruceRGB.icc
%{_icccolordir}/colord/CIE-RGB.icc
%{_icccolordir}/colord/Crayons.icc
%{_icccolordir}/colord/ColorMatchRGB.icc
%{_icccolordir}/colord/DonRGB4.icc
%{_icccolordir}/colord/ECI-RGBv1.icc
%{_icccolordir}/colord/ECI-RGBv2.icc
%{_icccolordir}/colord/EktaSpacePS5.icc
%{_icccolordir}/colord/Gamma*.icc
%{_icccolordir}/colord/NTSC-RGB.icc
%{_icccolordir}/colord/PAL-RGB.icc
%{_icccolordir}/colord/SwappedRedAndGreen.icc
%{_icccolordir}/colord/WideGamutRGB.icc

%files help
%doc README.md NEWS
%dir %{_datadir}/gtk-doc/html/colord
%{_datadir}/gtk-doc/html/colord/*
%{_datadir}/man/man1/*.1.gz

%changelog
* Fri Mar 13 2020 songnannan <songnannan2@huawei.com> - 1.4.3-6
- move the files to main package

* Mon Feb 17 2020 hexiujun <hexiujun1@huawei.com> - 1.4.3-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:unpack libs subpackage

* Tue Dec 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.4.3-4
- change the path of files

* Wed Sep 18 2019 Yiru Wang <wangyiru1@huawei.com> - 1.4.3-3
- Pakcage init
